$("#count-slider")
    .slider({
        formatter: function(value) {
            return "Current value: " + value;
        }})
    .on("slide", function(slideEvt) {
        $("#learning-slider-value").text(slideEvt.value);
    });

$("#settings").click(function() {
    $("#modal-settings").modal("show");
});
$("#learning-threshold-slider").css('background', 'bold');
$(".slider.slider-horizontal").css("width", "100%");


//Init variables
var categorizedEmailList = {};
categorizedEmailList[0] = {
    sender : "hh.ru",
    dateTime : "2015-02-24T21:23",
    subject : "Invitation for an interview",
    category : "Job"
};
categorizedEmailList[1] = {
    sender : "vk.com",
    dateTime : "2016-03-25T22:24",
    subject : "User Vasiliy Pupkin commented your photo",
    category : "Social"
};
categorizedEmailList[2] = {
    sender : "mailcat.com",
    dateTime : "2017-11-20T15:00",
    subject : "Nechaev Andrey made mailcat-frontend",
    category : "News"
};
categorizedEmailList[3] = {
    sender : "Elon Mask",
    dateTime : "2017-11-20T15:25",
    subject : "Invitation for an interview",
    category : "Job"
};

var tableCounter = 0;

var addEmailToTable = function(categorizedEmail, index){
    createContainer(index);
    setStyleToContainer(index);
    addDataToContainer(categorizedEmail, index);
};
var createContainer = function (index) {
    $("#inbox-list").append(
        "<div class=\"container-fluid\" id=\"email-list-item-" + index + "\">" +
            "<div class=\"row\">" +
                "<div class=\"col-3 email-field\">" +
                    "<span class=\"email-field\" id=\"category-" + index + "\"></span>" +
                "</div>" +
                "<div class=\"col-9\">" +
                    "<div class=\"row\">" +
                         "<div class=\"col-6 email-field\">" +
                            "<span id=\"sender-" + index + "\"></span>" +
                         "</div>" +
                        "<div class=\"col-6 email-field\">" +
                            "<span class=\"\" id=\"date-" + index + "\"></span>" +
                        "</div>" +
                    "</div>" +
                    "<div class=\"row\">" +
                        "<div class=\"col-12 email-field\">" +
                            "<span id=\"subject-" + index + "\"></span>" +
                        "</div>" +
                    "</div>" +
                "</div>" +
            "</div>" +
        "</div>"
    );
};
var addDataToContainer = function (categorizedEmail, index) {
    $("#sender-" + index).text(categorizedEmail.sender);
    $("#date-"  + index).text(categorizedEmail.dateTime);
    $("#subject-"  + index).text(categorizedEmail.subject);
    $("#category-"  + index).text(categorizedEmail.category);
};
var setStyleToContainer = function (index) {
    // $(".col-2")
    //     .css("padding-top", "8px")
    //     .css("padding-bottom", "8px");
    $("#category-" + index).parent()
        .css("padding-top", "8px")
        .css("padding-bottom", "8px");
    $("#date-" + index).parent()
        .css("text-align", "right");
    $("#email-list-item-" + index)
        .css("border", "2px groove black")
        // .css("border-style", "groove")
        .css("border-radius", "10px")
        .css("margin-bottom", "4px")
        .css("box-shadow",  "0 0 40px rgba(0, 0, 0, .2) inset");
    $(".email-field")
        .css("white-space", "nowrap")
        .css("text-overflow", "ellipsis")
        .css("overflow", "hidden");

};

$("#email-list-size").text(tableCounter);

$("#test-btn").on("click", function () {
    addEmailToTable(categorizedEmailList[tableCounter], tableCounter);
    tableCounter++;
    $("#email-list-size").text(tableCounter);
});


