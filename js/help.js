$("#count-slider")
    .slider({
        formatter: function(value) {
            return "Current value: " + value;
        }})
    .on("slide", function(slideEvt) {
        $("#learning-slider-value").text(slideEvt.value);
    });

$("#settings").click(function() {
    $("#modal-settings").modal("show");
});
$("#learning-threshold-slider").css('background', 'bold');
$(".slider.slider-horizontal").css("width", "100%");